terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}
provider "azurerm" {
  features {}
}


//Resource Group

resource "azurerm_resource_group" "demoRG" {
    name     = "RG1"
    location = "eastus"

    tags = {
        environment = "Terraform Demo"
    }
}

//virtual network

resource "azurerm_virtual_network" "demoVnet" {
    name                = "Vnet1"
    address_space       = ["10.0.0.0/16"]
    location            = "eastus"
    resource_group_name = azurerm_resource_group.demoRG.name

    tags = {
        environment = "Terraform Demo"
    }
}

//subnet

resource "azurerm_subnet" "demosubnet" {
    name                 = "subnet1"
    resource_group_name  = azurerm_resource_group.demoRG.name
    virtual_network_name = azurerm_virtual_network.demoVnet.name
    address_prefixes       = ["10.0.1.0/24"]
}

//public address

resource "azurerm_public_ip" "demoPubIP" {
    name                         = "PubIP1"
    location                     = "eastus"
    resource_group_name          = azurerm_resource_group.demoRG.name
    allocation_method            = "Dynamic"

    tags = {
        environment = "Terraform Demo"
    }
}


//Network Security group

resource "azurerm_network_security_group" "demoNSG" {
    name                = "NSG1"
    location            = "eastus"
    resource_group_name = azurerm_resource_group.demoRG.name

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags = {
        environment = "Terraform Demo"
    }
}

//Network Interface

resource "azurerm_network_interface" "demoNI" {
    name                        = "NI1"
    location                    = "eastus"
    resource_group_name         = azurerm_resource_group.demoRG.name

    ip_configuration {
        name                          = "myNicConfiguration"
        subnet_id                     = azurerm_subnet.demosubnet.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = azurerm_public_ip.demoPubIP.id
    }

    tags = {
        environment = "Terraform Demo"
    }
}


//Virtual Machine

resource "azurerm_linux_virtual_machine" "demoVM" {
    name                  = "VM1"
    location              = "eastus"
    resource_group_name   = azurerm_resource_group.demoRG.name
    network_interface_ids = [azurerm_network_interface.demoNI.id]
    size                  = "Standard_DS1_v2"

    os_disk {
        name              = "myOsDisk"
        caching           = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    source_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "18.04-LTS"
        version   = "latest"
    }

    computer_name  = "myvm"
    admin_username = "azureuser"
    disable_password_authentication = true

    admin_ssh_key {
        username       = "azureuser"
        public_key     =  "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDKEUgdbRONkOKOwGqEDi5wG5nhWe4eowObNZd1TG2jtZ6IyMp+OFNurHNLVnEUVb6iJus6qI6LNAlNZTUD/M0SVq43Kyk8mZyrvevY0wfSeZZaC5aM7FyERwjyrWVl4m99oY+aNkUNRhmuUxmsqxGnZ9LY3gIZ6oPFWpquEfR6/0k2dEjclT2vxAXnUtnsdWUAdRV3VVtJSDpZFbjE+u+HvcuDpB9oyiZiijoLXtB3BVsYeiSi4+x5F/U4OgZfhATrlPt7D9hqurVuhNrYcaeP0VKlmkc01p6hgNKfwxcodpBuOIu3qPo7dZ/tc1JtQ57r1XNcBUt99pAOTE9H8qzAiOQ/u4+cD2or9ZNU78rtJXoyDHqj6CX/3Ey0Q1VHH2XbPu/YBdBxmVYfAz3UtiayViy24CKMb+8HZ3lMWlZeVha0oI5xFN0c36mz6yPXPM9NgFzm10IpcoawYjlDKidPmuEKIT8h83Q9NkA7oUvMXesnxkk8CuoIJlUuZxOcN00= root@Demo-VM"
    }

    tags = {
        environment = "Terraform Demo"
    }
}
